<?php defined('ACCESS') OR die('No direct script access.');

abstract class Parashute
{
	public $name;//название
	public $Fn;//площадь купола
	public $massa;//масса снаряженной парашютной системы 
	public $Cp;//коэффициент Сп (массив)
	
	//todo: 
	public $lks; //длина купола, строп, соединит. звена (С-5К: 6 + 4.385 + 1.54)
	public $mpch; //масса парашюта, чехла,  соединит. звена (С-5К: 5.8 + 0.49 + 0.064)
}

class DrawParashute extends Parashute /* вытяжной парашют */
{
	public function __construct($name, $Fn, $massa, $Cp)
	{
		$this->name = $name;
	    $this->Fn = $Fn;
		$this->massa = $massa;
		$this->Cp = $Cp;
	}	
}

class ManParashute extends Parashute /* характеристики парашютной системы */
{	
	public $K;//коэффициент К (массив)
	public $Vpr;//скорость приземления	
	public $m = 2;//запас прочности
	public $tz;//задержка раскрытия
	
	public $VP;//вытяжной парашют
		
	private function __construct($name, $Fn, $massa, $Cp, $K, $Vpr, $tz)
	{
		$this->name = $name;
	    $this->Fn = $Fn;
		$this->massa = $massa;
		$this->Cp = $Cp;
	    $this->K = $K;
		$this->Vpr = $Vpr;
		$this->tz = $tz;		
		//$this->VP = $this->_getV($name); 
	}
	
	/**
	* 
	*/	
	public static function createParashute($name)
	{
		switch($name)
		{
			case "S5K": 
				return new ManParashute("С-5К (С-5И, ПСУ-36)", 	//название
							60, 								//площадь, м2
							25,									//масса снаряженного парашюта
							array(0.6, 0.75, 0.8), 				//коэффициент Сп 
							array(0.006, 0.0056, 0.005), 		//коэффициент К (для арт. 56008П К = 0.0056: см. ССЄС, стр. 115)
							6,									//скорость приземления, м/с
							2);								//задержка раскрытия, 
				break;
			case "S4U":
				return new ManParashute("С-4У", 			//название
							54, 								//площадь, м2
							18.5,								//масса снаряженного парашюта
							array(0.7, 0.84, 0.9), 				//коэффициент Сп 
							array(0.006, 0.0056, 0.005), 		//коэффициент К (для арт. 56008П К = 0.0056: см. ССЄС, стр. 115)
							6,									//скорость приземления, м/с
							3);
				break;
			case "PL70":
				return new ManParashute("ПЛ-70", 			//название
							50, 								//площадь, м2
							35,								//масса снаряженного парашюта
							array(0.8, 0.9, 1), 				//коэффициент Сп 
							array(0.006, 0.0056, 0.005), 		//коэффициент К (для арт. 56008П К = 0.0056: см. ССЄС, стр. 115)
							6.5,									//скорость приземления, м/с
							0.5);
				break;			
		}
	}
	
	/**
	* 
	*/
	private function _getVP($name)
	{
		switch($name)
		{
			case "С-5К": 
				return new DrawParashute("вытяжной 0.48 м2", 0.48, 0.085, 0.8);
				break;
			case "С-4У":
				return new DrawParashute("вытяжной 0.98 м2", 0.98, 0.17, 0.8);//отсутствует инфо о массе
				break;
			case "ПЛ-70":
				return new DrawParashute("вытяжной 0.8 м2", 0.8, 0.15, 0.8);//отсутствует инфо о массе, + стабилизирующий парашют 1,5 м2
				break;
			default:
				return new DrawParashute("стабилизирующий 1.5 м2", 1.5, 0.28, 0.8);
		}
	
	}
	
	public function __destruct()
	{
		//unset($this->VP);
	}	
}

