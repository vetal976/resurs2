<?php defined('ACCESS') OR die('No direct script access.');

class CountSpeed /*Розрахунок швидкості системи в момент введення*/
{
	private $V;//результат расчета
	
	public function __construct(LeaveConditions $LC, CrewMember $crewmember)
	{
		$this->V = $this->countVt($LC, $crewmember);
	}
	
	public function getV()
	{
		return $this->V;
	}	
	
	/**
	* расчет критической скорости объекта в зависимости от высоты (м/с)
	*/
	public function _countVkr(DetachObject $object, $H)
	{
		$m = $object->m;
		$cf = $object->cf;
		return sqrt(2*$m/(CA::getRo($H)*$cf));		 
	}

	/** 
	* расчет скорости системы в момент начала наполнения в зависимости от  
	* задержки раскрытия, начальной скорости и высоты;
	* вывод результатов
	*/
	private function countVt(LeaveConditions $LC, CrewMember $crewmember)
	{
		$Vnach = $this->_correctV($LC);//перевод приборной в истинную скорость
		$Hnach = $LC->H;
		
		if (!is_null($KK = $LC->KK))//если покидание катапультированием
		{
			$KK->m += $crewmember->m;//корректировка массы
			$Vkr = $this->_countVkr($KK, $Hnach);
			$Vnach = $this->_countVt($Vkr, $Vnach/=3.6, $KK->tz);
			$Hnach += 0; //корректировка высоты /todo:
		}		
		$Vkr = $this->_countVkr($crewmember, $Hnach);
		//без коректировки определяется скорость в момент открытия ранца
		//поэтому к tz прибавим время до вытягивания чехла на всю длину
		$Tv = $this->_countTv($crewmember, new ImplementConditions($Hnach, $Vnach));
		$tz = $crewmember->PS->tz + $Tv;
		$Vt = $this->_countVt($Vkr, $Vnach/=3.6, $tz);	
		/* ------ расчет для ПЛ-70 c задержкой 1,5 с ---- /
			$Vt = $this->_countVt($Vkr, $Vnach/=3.6, 0.5);
			$crewmember->cf = 0.64;//вытяжной парашют
			$Vkr = $this->_countVkr($crewmember, $Hnach);
			$Vt = $this->_countVt($Vkr, $Vt/=3.6, 0.3);
			$crewmember->cf = 1.2;//стабилизирующий парашют
			$Vkr = $this->_countVkr($crewmember, $Hnach);
			$Vt = $this->_countVt($Vkr, $Vt/=3.6, 0.7);
		/ ---------- конец расчета для ПЛ-70 ----------- */
		return $Vt;
	}
	
	/**
	* определение времени вытягивания купола и строп на всю длину
	*/
	private function _countTv(CrewMember $crewmember, ImplementConditions $IC)
	{
		//todo: write code
		
		return 0.3; //(0.4 - Ан-70, V = 530 км/ч)		
	}
	
	/**
	* расчет скорости (Лобанов, стр. 28)
	*/
	private function _countVt($Vkr, $Vnach, $t)
	{
		$Vt = $Vkr*(($Vkr + $Vnach)*exp(2*9.81*$t/$Vkr) - ($Vkr - $Vnach))/(($Vkr + $Vnach)*exp(2*9.81*$t/$Vkr) + ($Vkr - $Vnach));
		return $Vt*3.6; 
	}
	
	/**
	* перевод приборной в истинную скорость (сжимаемость воздуха не учитывается)
	*/
	private function _correctV (LeaveConditions $LC)
	{
		$V_arr = array('350'=>array('2000'=>383, '3000'=>404, '4000'=>324, '5000'=>446, '6000'=>472, '7000'=>497, '8000'=>523, '9000'=>553,'10000'=>587, '11000'=>620, '12000'=>666),
			'450'=>array('2000'=>493, '3000'=>519, '4000'=>543, '5000'=>572, '6000'=>603, '7000'=>634, '8000'=>668, '9000'=>703,'10000'=>744, '11000'=>784, '12000'=>841),
			'500'=>array('2000'=>547, '3000'=>575, '4000'=>603, '5000'=>632, '6000'=>667, '7000'=>700, '8000'=>736, '9000'=>774,'10000'=>818, '11000'=>862, '12000'=>922),
			'600'=>array('2000'=>656, '3000'=>690, '4000'=>721,'5000'=>756, '6000'=>789, '7000'=>833, '8000'=>875, '9000'=>918,'10000'=>967, '11000'=>1015, '12000'=>1084),
			'900'=>array('2000'=>977, '3000'=>1020, '4000'=>1061,'5000'=>1107, '6000'=>1156, '7000'=>1205, '8000'=>1260, '9000'=>1317,'10000'=>1380, '11000'=>1452, '12000'=>1635),						
			'1100'=>array('2000'=>1187, '3000'=>1235, '4000'=>1285,'5000'=>1335, '6000'=>1398, 	'7000'=>1456,'8000'=>1525, '9000'=>1600,'10000'=>1682, '11000'=>1772, '12000'=>1900)		
		);
				
		$V = $LC->Vnach; $H = $LC->H;
		
		if($LC->H >= 2000 && $LC->pr != false)
		{
			if(array_key_exists( $V.'', $V_arr) && array_key_exists( $H.'', $V_arr[$V]))
			{
				$V = $V_arr[$V][$H];
			}
			else
			{			
				echo "Отсутствует значение истинной скорости для заданного значения приборной"; exit();
				//$V = $V/sqrt(CA::getDelta($LC->H));
			}	
		}
		return $V;		
	}
	
	/**
	* расчет пути, проходимого телом при вертикальном падении за время t
	*/
	private function _countHt($Vnach, $Vkr, $t)
	{
		$Ht = (pow($Vkr, 2)/9.81)*log((($Vkr + $Vnach)*exp(2*9.81*$t/$Vkr) + ($Vkr - $Vnach))/(2*$Vkr*exp(9.81*$t/$Vkr)));
		return $Ht;
	}	
}