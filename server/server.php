<?php
define ('ACCESS', 1);
require_once 'lib/classes.php';

$requestBody = file_get_contents('php://input');
$request = json_decode($requestBody);
$leave_cases = array();//случаи покидания

/* ---- создание и инициализация объектов --------- */
foreach ($request as $req)
{    
	$leave_cases[] = array('PS' =>ManParashute::createParashute($req->PS), 
						'LC' =>new LeaveConditions((integer)$req->H, 	//высота м
									(integer)$req->V, 				//скорость км/ч
									($req->speedType == "prib") ? true : false,	//истинная (false), приборная (true) скорость 
									($req->leaveMethod == "inject") ? new Catapult(1.5) : null //катапультирование или прыжок  
	));
}

/* -------------------- расчет ------------------- */
//foreach($req as $case)
foreach($leave_cases as $case)
{	
	$determine = new StrengthDetermine($case['PS'], $case['LC']);
	$determine->getResults();
	$determine->printResults();
	/*----------------------------------------*/
	unset($determine);	unset($case['PS']); unset($case['LC']); 
}
/* ----------------------------------------------- */

