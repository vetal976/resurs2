<?php defined('ACCESS') OR die('No direct script access.');
/**
* загрузка классов, используемых в приложении:
* 'ca.php' - стандартная атмосфера
* 'countspeed.php' - Розрахунок швидкості системи в момент введення
* 'parashute.php' - характеристики парашютной системы 
* 'leaveconditions.php' - условия ввода в действие 
* 'strengthdetermine.php' - расчет показателей 
*/
spl_autoload_register(function ($class) {
    require_once $class.'.php';
});
/*
function __autoload($name)
{
	require_once $name.".php";
}*/

class DetachObject /* отделяемый от ЛА объект */
{
	public $m; //вес 
	public $cf; //произведение коэффициента сопротивления на площадь миделя
	
	public function __construct($m, $cf)
	{
		$this->m = $m;
		$this->cf = $cf;
	}
}

class  CrewMember extends DetachObject /* член экипажа */
{
	public $PS;//спасательная парашютная система
		
	public function __construct(Parashute $PS)
	{
		parent::__construct(100, 0.4);// 0.4 стр. 87 - Средства спасения экипажа самолета. М., "Машиностроение", 1975, 432 с
		$this->PS = $PS;
		$this->m += $PS->massa;//прибавляем массу парашютной системы	//todo:	
	}
}

class Catapult extends DetachObject /* катапульное кресло */
{	
	public $tz; //время спустя которое член экипажа отделяется от кресла (секунд)  
	
	public function __construct($tz)
	{
		parent::__construct(122, 0.5);//К-36ДМ + стр. 157 - Средства спасения экипажа самолета. М., "Машиностроение", 1975, 432 с
		//ВС1-БРИ - 80 кг
		$this->tz = $tz;
	}
	
	
}