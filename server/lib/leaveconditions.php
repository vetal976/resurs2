<?php defined('ACCESS') OR die('No direct script access.');

class FlightParams /* параметры полета */
{
	public $H;//высота покидания (!значение должно быть одним из ключей массива СА::$ro_arr)
	public $Vnach; // скорость ЛА при покидании (км/ч)
	public $pr; //истинная - false, приборная true
	
	public function setParams($H, $Vnach, $pr = false)
	{
		$this->H = $H;
		$this->Vnach = $Vnach;
		$this->pr = $pr;
	}
}

class LeaveConditions extends FlightParams /* условия покидания ЛА */
{
	public $KK; //катапульное кресло (прыжок или катапультирование)
	
	public function __construct($H, $Vnach, $pr = false, Catapult $KK = null)
	{
		parent::setParams($H, $Vnach, $pr);
		$this->KK = $KK;
	}	
}

class ImplementConditions extends FlightParams /* условия введения в действие */
{
	/*
	public static function getInstance(LeaveConditions $LC, CrewMember $crewmember)
	{
		//todo: write code
		$H = 500; 
		$Vnach = 400;
		return new self($H, $Vnach);
	}*/
}