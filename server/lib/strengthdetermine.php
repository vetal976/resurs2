<?php defined('ACCESS') OR die('No direct script access.');

class StrengthDetermine /* расчет показателей */
{
	private $V0;//скорость системы при введении в действие
	private $PS;//парашютная система
	private $LC;//условия покидания
	
	public $data = array(); //результаты расчета
	
	public function __construct(Parashute $PS, LeaveConditions $LC)
	{
		$speed_counter = new CountSpeed($LC, new CrewMember($PS));//todo:
		$this->V0 = $speed_counter->getV();//определение скорости системы в начале наполнения
		unset($speed_counter);
		
		$this->PS = $PS;
		$this->LC = $LC;			
	}
	
	/**
	* Расчет Rmax, fop, dop, sigma_vmax, необходимой прочности ткани
	*/
	public function getResults()
	{		
		$Vpr = $this->PS->Vpr; 					// инициализация
		$m = $this->PS->m;						//переменных
		$Fn = $this->PS->Fn;					//которые
		$V0 = $this->V0/3.6;					//используются
		$delta = CA::getDelta($this->LC->H);	//в 
		$ro	= CA::getRo($this->LC->H);		//формулах
		$nju = 1;		

		foreach ($this->PS->K as $k)
		{						
			foreach ($this->PS->Cp as $c)
			{
				$Cf = $c/2;
				$data = &$this->data["$k"];//псевдоним 
				$data['Rmax']["$c"] = $Rmax = (pow($Vpr, 2)/8)*$c*$Fn*($k*pow($V0, 2)+sqrt($Fn))/($k*pow($Vpr, 2)/$delta+2*$nju*sqrt($Fn));
				$fop1 = $Rmax/($ro*pow($V0, 2)*$Cf);
				$kvVpr2 = pow($Vpr, 2)*$Fn/$fop1;
				$Rmax2 = ($kvVpr2/8)*$c*$fop1*($k*pow($V0, 2)+sqrt($fop1))/($k*$kvVpr2/$delta+2*sqrt($fop1));
				$data['Cf']["$c"] = $Cf = $c*$Rmax/$Rmax2;
				$data['fop']["$c"] = $fop = $Rmax/($ro*pow($V0, 2)*$Cf);
				$data['dop']["$c"] = $dop = sqrt(4*$fop/3.141592654);
				$data['sigma_vmax']["$c"] = $sigma_vmax = $Rmax/(2*$dop);
				$data['sigma_tk']["$c"] = $sigma_tk = $sigma_vmax*$m;
				$data['prochn']["$c"] = $prochn = $sigma_tk/20;				
			}// по коэффициенту Сп					
		} // по коэффициенту К		
	}
	
	/**
	* вывод результатов расчета 
	*/
	public function printResults()
	{
		echo '<div class = "for_PS">';
		echo '<b>'; printf ("Парашутна система %s",$this->PS->name); echo '</b>'; echo "<br>";
		$pr = ($this->LC->pr == true) ? 'ПР': '';//приборная или истинная
		$kat = ($this->LC->KK !== null) ? 'катапультування,': '';
		printf("Умови застосування: {$kat} швидкість - V = %d {$pr} км/год; висота - Н = %d м; tз = %.1f с", $this->LC->Vnach, $this->LC->H, $this->PS->tz); echo '<br>';
		
		echo '<span style = "font-size: 0.75em">';
			//printf("(delta = %.5f; ro = %.5f)", $this->delta, $this->ro);
		echo '</span>';
			
		echo '<div class = "for_V">';
		echo '<span style = "margin-left: 60px; ">';
			printf("Швидкість системи в момент початку наповнення Vпн = %.2f м/с;", $this->V0/3.6); echo "<br>";echo "<br>";
		echo '</span>';
		foreach($this->data as $key_k=>$k)
		{	
			echo '<div style = "clear: both;">';
			echo '<div>'."Таблиця ____ - Результати розрахунку при К = ".$key_k.'</div>'; 
			echo '<table rules ="cols">';
			/*--- заголовок таблицы ---*/						
			echo '<tr>';
			echo '<th>Параметр</th>';
			$Cp_arr = &$k['Rmax'];
			foreach($Cp_arr as $key=>$value)
			{
				echo '<th> Сп = '.$key.'</th>';
			}					
			echo '</tr>';
			/*---- тело таблицы ---*/			
			foreach($k as $key_k=>$c)
			{				
				echo '<tr>';
				echo '<td>'.$key_k.'</td>';
				foreach($c as $value)
				{
					echo '<td>';
					printf("%.3f", $value);
					echo '</td>';
				}	
				echo '</tr>';			
			}
			/*---              ---*/
			echo '</table>'; echo "<br>"; echo "<br>";
			echo '</div>';	
		}
		echo '</div>'; echo "<br>";
		echo '</div>';	
	}	
}