console.log("Script was connected");
var leaveCases = [];

window.onload = function(){
	document.getElementById('calculate').onclick = function(){
		var request = JSON.stringify(leaveCases);
		sendRequest(request, function(data){
			document.body.innerHTML = data;
		});
	};
	
	document.getElementById('add-case').onclick = function(){
		var form = document.getElementsByTagName("form")[0];
		var leaveCase = {
			PS: form.PS.value,
			H: form.H.value,
			V: form.V.value,
			speedType: form['speed-type'].value,
			leaveMethod: form['leave-method'].value
		};
		leaveCases.push(leaveCase);
		renderLeaveCases();
		console.log(leaveCase);
	};	
};

function renderLeaveCases(){
	document.getElementById('leave-cases').innerHTML = '';
	for (var i = 0, j = leaveCases.length; i < j; i++){
		var p = document.createElement('p');
		p.innerHTML = 'ПС: ' + leaveCases[i].PS + ' H = ' + leaveCases[i].H + ' V = ' + leaveCases[i].V;
		document.getElementById('leave-cases').appendChild(p); 
	}	
}

/**/
function sendRequest (request, callback) {
	var contentType = 'application/x-www-form-urlencoded';//todo:
	
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
	    var xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5
		var xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	xmlhttp.onreadystatechange = function(){
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
			callback(xmlhttp.responseText);			
		}
	}
	
	xmlhttp.open("POST","server/server.php", true);
	xmlhttp.setRequestHeader('Content-Type', contentType);
	xmlhttp.send(request);
}